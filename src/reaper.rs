use std::sync::Arc;

use crate::thread::Thread;

#[derive(Clone, Debug)]
pub(super) struct Reaper {
    _thread: Arc<Thread>,
    sender: flume::Sender<Thread>,
}

impl Reaper {
    pub(super) fn new() -> Self {
        let (sender, rx) = flume::unbounded();

        let thread = Arc::new(Thread::build(String::from("vectordb-reaper")).spawn(
            move |stopper| {
                crate::with_stopper(rx, stopper, |thread: Thread| {
                    if let Err(e) = thread.shutdown() {
                        tracing::error!("Source thread panicked: {e:?}");
                    }
                })
            },
        ));

        Self {
            _thread: thread,
            sender,
        }
    }

    pub(super) fn send(&self, thread: Thread) -> Result<(), Thread> {
        self.sender.send(thread).map_err(|e| e.into_inner())
    }
}
